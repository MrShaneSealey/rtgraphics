#version 460

uniform sampler2D texture;

in vec3 texCoord;
in vec4 position;
in vec3 pixelNormal;

layout (location=0) out vec4 fragColour;

vec3 calcLightContributionForLight(vec3 vertexPos, vec3 vertexNormal, vec3 L, vec3 Lcolour, vec3 LK){
	
	vec3 D = L - vec3(position.xyz);
	float lengthD = length(D);
	vec3 D_ = normalize(D);

	float a = 1.0/(LK.y * lengthD + LK.z * lengthD * lengthD);

	float lambertian = clamp(dot(D_, pixelNormal), 0.0,1.0);

	return lambertian * a * Lcolour;
}

void main(void) {

	vec3 L[2];
	L[0] = vec3(-5.0,10.0,-7.0);
	L[1] = vec3(5.0,16.0,2.0);

	vec3 Lcol[2];
	Lcol[0] = vec3(1.2,1.0,0.5);
	Lcol[1] = vec3(0.2,1.5,1.5);

	vec3 LK1[2];
	LK1[0] = vec3(1.0,0.1,0.0);
	LK1[1] = vec3(1.0,0.1,0.0);

	vec3 brightness = vec3(0.0,0.0,0.0);
	for(int i = 0; i<2; i++){
		brightness += calcLightContributionForLight(position.xyz,pixelNormal,L[i],Lcol[i],LK1[i]);
	}

	vec4 texColor = texture2D(texture, texCoord.xy) * vec4(brightness,1.0);
	texColor.a = 0.5;
	fragColour = texColor;
    
}