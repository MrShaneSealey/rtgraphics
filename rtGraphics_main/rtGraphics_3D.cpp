// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include <glew/glew.h>
#include <glew/wglew.h>
#include <GL\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include <string>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include "src/CGTexturedQuad.h"
#include "src/aiWrapper.h"
#include "src/CGPrincipleAxes.h"
#include "src/texture_loader.h"
#include "src/sceneVBOs.h"
#include "src/GURiffModel.h"
#include "al.h"
#include "alc.h"
#include "efx.h"
#include "EFX-Util.h"
#include "efx-creative.h"
#include <xram.h>
#include <mmreg.h>


using namespace std;
using namespace CoreStructures;

#pragma region Scene variables and resources

// Variables needed to track where the mouses pointer is so we can determine which direction it's moving in
int	mouse_x, mouse_y;
bool keyDownW, keyDownS, keyDownA, keyDownD, keyDownQ, keyDownE = false;
bool mDown = false;

// Player Varibles
float playerX = 0.0f;
float playerY = 2.5f;
float playerZ = 0.0f;

//Camera Movement
float cameraX = 0.0f;
float cameraY = -1.0f;
float cameraZ = 0.0f;

//rotation
float pRotationX = 0.0f;
float pRotationY = 0.0f;
float pRotationZ = 0.0f;

// playerFire Varibles
bool playerFire = false;
//Animation Control
float pScaleX = 0.3f;
float pScaleY = 0.3f;
float pScaleZ = 0.3f;

GUClock* mainClock = nullptr;

// Main scene resources
GUPivotCamera* mainCamera = nullptr;
CGPrincipleAxes* principleAxes = nullptr;
CGTexturedQuad* texturedQuad = nullptr;

//Tank Setup
const aiScene* aiTank;
GLuint tankTexture;
sceneVBOs* Tank;

//Background Setup
const aiScene* aiMap;
GLuint mapTexture;
sceneVBOs* Map;

//Obelisk
const aiScene* aiObelisk;
GLuint obeliskTexture;
sceneVBOs* Obelisk;

//Tower
const aiScene* aiTower;
GLuint towerTexture;
sceneVBOs* Tower;

GLuint meshShader;
#pragma endregion


#pragma region Function Prototypes

void init(int argc, char* argv[]); // Main scene initialisation function
void update(void); // Main scene update function
void display(void); // Main scene render function

// Event handling functions
void mouseButtonDown(int button_id, int state, int x, int y);
void mouseMove(int x, int y);
void mouseWheel(int wheel, int direction, int x, int y);
void keyDown(unsigned char key, int x, int y);
void closeWindow(void);
void reportContextVersion(void);
void reportExtensions(void);

#pragma endregion


int main(int argc, char* argv[])
{
	init(argc, argv);
	glutMainLoop();

	// Stop clock and report final timing data
	if (mainClock) {

		mainClock->stop();
		mainClock->reportTimingData();
		mainClock->release();
	}

	return 0;
}


void init(int argc, char* argv[]) {

	// Initialise FreeGLUT
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE/* | GLUT_MULTISAMPLE*/);
	glutSetOption(GLUT_MULTISAMPLE, 4);

	// Setup window
	int windowWidth = 800;
	int windowHeight = 600;
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(64, 64);
	glutCreateWindow("3D Example 01");
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	// Register callback functions
	glutIdleFunc(update); // Main scene update function
	glutDisplayFunc(display); // Main render function
	glutKeyboardFunc(keyDown); // Key down handler
	glutMouseFunc(mouseButtonDown); // Mouse button handler
	glutMotionFunc(mouseMove); // Mouse move handler
	glutMouseWheelFunc(mouseWheel); // Mouse wheel event handler
	glutCloseFunc(closeWindow); // Main resource cleanup handler


	// Initialise glew
	glewInit();

	// Initialise OpenGL...

	wglSwapIntervalEXT(0);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glFrontFace(GL_CCW); // Default anyway

	// Setup colour to clear the window
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Setup main camera
	float viewportAspect = (float)windowWidth / (float)windowHeight;
	mainCamera = new GUPivotCamera(0.0f, 0.0f, 15.0f, 55.0f, viewportAspect, 0.1f);

	principleAxes = new CGPrincipleAxes();

	//Tank Setup
	aiTank = aiImportModel(string("..\\Common2\\Resources\\Tank.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);
	Tank = new sceneVBOs(aiTank);
	tankTexture = fiLoadTexture("..\\Common2\\Resources\\Textures\\TankTexture.png", TextureProperties(false));

	//Map Setup
	aiMap = aiImportModel(string("..\\Common2\\Resources\\Map.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);
	Map = new sceneVBOs(aiMap);
	mapTexture = fiLoadTexture("..\\Common2\\Resources\\Textures\\Map UvMap.png", TextureProperties(false));

	//obelisk Setup
	aiObelisk = aiImportModel(string("..\\Common2\\Resources\\Obelisk.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);
	Obelisk = new sceneVBOs(aiObelisk);
	obeliskTexture = fiLoadTexture("..\\Common2\\Resources\\Textures\\Obelisk UVMAP.png", TextureProperties(false));

	//Tower setup
	aiTower = aiImportModel(string("..\\Common2\\Resources\\Aswan Tower.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);
	Tower = new sceneVBOs(aiTower);
	towerTexture = fiLoadTexture("..\\Common2\\Resources\\Textures\\Tower UvMap.png", TextureProperties(false));

	meshShader = setupShaders(string("shaders\\mesh_shader.vs"), string("shaders\\mesh_shader.fs"));

	// ~~ ## SOUND Setup ## ~~ //
	ALCdevice* alcDevice = alcOpenDevice(NULL);
	ALCcontext* alcContext = nullptr;

	alcContext = alcCreateContext(alcDevice, NULL);
	alcMakeContextCurrent(alcContext);

	ALuint buffer1;
	alGenBuffers(1, &buffer1);

	auto mySoundData = new GURiffModel("..\\Common2\\Resources\\Sound\\EpicTankGame.wav");

	RiffChunk formatChunk = mySoundData->riffChunkForKey('tmf');
	RiffChunk dataChunk = mySoundData->riffChunkForKey('atad');

	WAVEFORMATEXTENSIBLE wv;
	memcpy_s(&wv, sizeof(WAVEFORMATEXTENSIBLE),formatChunk.data,formatChunk.chunkSize);

	alBufferData(
		buffer1,
		AL_FORMAT_MONO16,
		(ALvoid*)dataChunk.data,
		(ALsizei)dataChunk.chunkSize,
		(ALsizei)wv.Format.nSamplesPerSec
	);

	ALuint source1;

	alGenSources(1, &source1);

	//Attach buffer
	alSourcei(source1, AL_BUFFER, buffer1);

	alSource3f(source1, AL_POSITION, 1.0f, 0.f, 0.0f);
	alSource3f(source1, AL_VELOCITY, 0.0f, 0.0f, 0.0f);
	alSource3f(source1, AL_DIRECTION, 0.0f, 0.0f, 0.0f);

	auto cameraLocation = mainCamera->cameraLocation();

	ALfloat listenerVel[] = { 0.0f, 0.0f, 0.0f };
	ALfloat listnerOri[] = { 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f };
	
	alListenerfv(AL_POSITION, (ALfloat*)(&cameraLocation));
	alListenerfv(AL_VELOCITY, listenerVel);
	alListenerfv(AL_ORIENTATION, listnerOri);

	alSourcePlay(source1);

	// Setup and start the main clock
	mainClock = new GUClock();
}

// Main scene update function (called by FreeGLUT's main event loop every frame) 
void update(void) {

	// Update clock
	mainClock->tick();

	// Redraw the screen
	display();

	// Update the window title to show current frames-per-second and seconds-per-frame data
	char timingString[256];
	sprintf_s(timingString, 256, "CIS5013. Average fps: %.0f; Average spf: %f", mainClock->averageFPS(), mainClock->averageSPF() / 1000.0f);
	glutSetWindowTitle(timingString);

	//PlayerMove

	if (keyDownW == true)
	{
		playerZ = 0.2f + playerZ;
		cameraZ = -0.06f + cameraZ;
		keyDownW = false;
	}

	if (keyDownS == true)
	{
		playerZ = -0.2f + playerZ;
		cameraZ = 0.06f + cameraZ;
		keyDownS = false;
	}

	if (keyDownA == true)
	{
		playerX = 0.1f + playerX;
		cameraX = -0.03f + cameraX;
		keyDownA = false;
	}

	if (keyDownD == true)
	{
		playerX = -0.1f + playerX;
		cameraX = 0.03f + cameraX;
		keyDownD = false;
	}

	if (keyDownE == true)
	{
		pRotationY = 0.2f + pRotationY;
		keyDownE = false;
	}

	if (keyDownQ == true)
	{
		pRotationY = -0.2f + pRotationY;
		keyDownQ = false;
	}

	//PlayerFire

}

void display(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set viewport to the client area of the current window
	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

	// Get view-projection transform as a GUMatrix4
	GUMatrix4 T = mainCamera->projectionTransform() * mainCamera->viewTransform() * GUMatrix4::translationMatrix(cameraX, cameraY, cameraZ);

	if (principleAxes)
		principleAxes->render(T);

	if (texturedQuad)
		texturedQuad->render(T);

	// Render example model loaded from obj file

	static GLuint mvpLocation = glGetUniformLocation(meshShader, "mvpMatrix");

	// Ground
	glBindTexture(GL_TEXTURE_2D, mapTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 mapMatrix = GUMatrix4::scaleMatrix(5.0, 5.0, 5.0)* GUMatrix4::translationMatrix(0.0, -0.25, 0.0);
	GUMatrix4 T5 = T * mapMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T5.M));
	Map->render();

	// Tank
	glBindTexture(GL_TEXTURE_2D, tankTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 TankMatrix = GUMatrix4::scaleMatrix(pScaleX, pScaleY, pScaleZ)*GUMatrix4::translationMatrix(playerX,playerY,playerZ)*GUMatrix4::rotationMatrix(pRotationX,pRotationY,pRotationZ);//xyz
	GUMatrix4 T1 = T * TankMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T1.M));
	Tank -> render();

	// ## - OBJECTS = ## //

	glBindTexture(GL_TEXTURE_2D, towerTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 TowerMatrix = GUMatrix4::scaleMatrix(1.0, 1.0, 1.0) * GUMatrix4::translationMatrix(0.0, 0.0, 0.0);//xyz
	GUMatrix4 T2 = T * TowerMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T2.M));
	Tower->render();

	glBindTexture(GL_TEXTURE_2D, obeliskTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 ObeliskMatrix = GUMatrix4::translationMatrix(0.0, 0.0,0.0);//xyz
	GUMatrix4 T3 = T * ObeliskMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T3.M));
	Obelisk->render();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);
	glBindTexture(GL_TEXTURE_2D, obeliskTexture);
	glColor4f(1.0, 1.0, 1.0, 0.2);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 ObeliskMatrix2 = GUMatrix4::translationMatrix(5.0, 0.0, 5.0);
	GUMatrix4 T4 = T * ObeliskMatrix2;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T4.M));
	Obelisk->render();
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

	glutSwapBuffers();
}



#pragma region Event handling functions

void mouseButtonDown(int button_id, int state, int x, int y) {

	if (button_id == GLUT_LEFT_BUTTON) {

		if (state == GLUT_DOWN) {

			mouse_x = x;
			mouse_y = y;

			mDown = true;

		}
		else if (state == GLUT_UP) {

			mDown = false;
		}
	}
}


void mouseMove(int x, int y) {

	int dx = x - mouse_x;
	int dy = y - mouse_y;

	if (mainCamera)
		mainCamera->transformCamera((float)-dy, (float)-dx, 0.0f);

	mouse_x = x;
	mouse_y = y;
}


void mouseWheel(int wheel, int direction, int x, int y) {

	if (mainCamera) {

		if (direction < 0)
			mainCamera->scaleCameraRadius(1.1f);
		else if (direction > 0)
			mainCamera->scaleCameraRadius(0.9f);
	}
}


void keyDown(unsigned char key, int x, int y) {

	// Toggle fullscreen (This does not adjust the display mode however!)
	if (key == 'f')
		glutFullScreenToggle();

	// Movement
	if (key == 'w') {
		keyDownW = true;
	}

	if (key == 's') {
		keyDownS = true;
	}
	if (key == 'a'){
		keyDownA = true;
	}

	if (key == 'd'){
		keyDownD = true;
	}

	if (key == 'q'){
		keyDownQ = true;
	}

	if (key == 'e') {
		keyDownE = true;
	}
}


void closeWindow(void) {

	// Clean-up scene resources

	if (mainCamera)
		mainCamera->release();

	if (principleAxes)
		principleAxes->release();

	if (texturedQuad)
		texturedQuad->release();

	//if (exampleModel)
		//exampleModel->release();
}


#pragma region Helper Functions

void reportContextVersion(void) {

	int majorVersion, minorVersion;

	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

	cout << "OpenGL version " << majorVersion << "." << minorVersion << "\n\n";
}

void reportExtensions(void) {

	cout << "Extensions supported...\n\n";

	const char* glExtensionString = (const char*)glGetString(GL_EXTENSIONS);

	char* strEnd = (char*)glExtensionString + strlen(glExtensionString);
	char* sptr = (char*)glExtensionString;

	while (sptr < strEnd) {

		int slen = (int)strcspn(sptr, " ");
		printf("%.*s\n", slen, sptr);
		sptr += slen + 1;
	}
}

#pragma endregion 